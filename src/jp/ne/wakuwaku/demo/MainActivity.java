package jp.ne.wakuwaku.demo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import jp.ne.wakuwaku.sdk.Wakuwaku;

public class MainActivity extends Activity {

    private final String TAG = "wakuwaku-portrait-demo";

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Wakuwaku.activateAsync(
                this,
                "9b20d219e157c51f13cdba2ca5b158dda4f1702c0ee34fe0059314fe65bd0b647b0f22b2ff7704d0064c48c120b7fa4940af19f8b2d62479944cb0785e7aa1d8",
                //"db1cbd84-cac2-4868-8519-4726b0641d65",
                "c0d70ff5-2aca-4116-afa7-e6173cdc7584",
                false,
                "portrait",
                true,
                false
                );

        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics size = new DisplayMetrics();
        Display dis = wm.getDefaultDisplay();
        dis.getMetrics(size);
        Log.d(TAG, "Display metricts width (x): " + size.widthPixels);
        Log.d(TAG, "Display metricts height (y): " + size.heightPixels);
        Log.d(TAG, "Display width (x): " + dis.getWidth());
        Log.d(TAG, "Display height (y): " + dis.getHeight());
    }

    public void doIt(View view) {
        Wakuwaku.couponDialog(this);
    }
}

