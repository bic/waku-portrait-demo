#!/usr/bin/env bash

set -e

if [ "$VERSION" = "" ]; then
	VERSION=0.0.3
fi
echo "Generationg v$VERSION"

if [ -f bin/WakuPortrait-release-$VERSION.apk ]; then
	read -p "bin/WakuPortrait-release-$VERSION.apk already exists, replace it? (y/N)" answer
	if [ "$answer" = "y" ]; then
		rm bin/WakuPortrait-release-$VERSION.apk
	else
		echo "Aborted"
		exit 1
	fi
fi

ant clean && ant release

cp bin/WakuPortrait-release-unsigned.apk bin/WakuPortrait-release-signed.apk
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ~/Dropbox/6_WakuWaku/android/wakuwaku-dev.keystore bin/WakuPortrait-release-signed.apk demo
jarsigner -verify bin/WakuPortrait-release-signed.apk

/Applications/Android/sdk/tools/zipalign -v 4 bin/WakuPortrait-release-signed.apk bin/WakuPortrait-release-$VERSION.apk

